import os 
import re
import sys


def ps():

	dict_process_cmd = {}

	regex = "PPid:\t([0-9]+)"


	pids=[]

	try :
		for root  in os.listdir("/proc"):

			x = re.search("[0-9]*",root)

			if(x.group()!=""):

				pids.append(x.group())

	except :
	
		pass
	

	for pid in pids:

		try:
			
			f = open("/proc/"+pid+"/cmdline","r")

			f1 = open("/proc/"+pid+"/status","r")
			
			result = str(f.read())
			
			result1 = str(f1.read())


			if(result!=""):

				if len(result.split("--")) > 0:

					cmd = result.split("--")[0]

				else:

					cmd = result
			else:

				matches = re.finditer("Name:\t([\w]+)", result1, re.MULTILINE)

				for matchNum, match in enumerate(matches, start=1):

					cmd = match.group(1)

			matches = re.finditer(regex, result1, re.MULTILINE)

			for matchNum, match in enumerate(matches, start=1):

				temp_ppid =match.group(1)
			
			dict_process_cmd[pid]=(temp_ppid,cmd)
			
			

			f.close()
			
			f1.close()

		except:
			
			pass

	return dict_process_cmd


def ps_print(dict_process_cmd,flag_all):

	keys = dict_process_cmd.keys()

	pid_bash = os.getppid()

	print("PID\tTTY\tCMD")

	bash_vect=[]

	for key in keys:

		ppid = dict_process_cmd[key][0]

		if  ppid in keys:
			
			if ppid.find(str(pid_bash)) == 0 or str(pid_bash).find(key) == 0 or flag_all=="-a":
			
				tty=get_tty(key,ppid,dict_process_cmd[ppid][1],
					bash_vect,dict_process_cmd[key][1])

				print("%s\t%s\t%s"%(key,tty,dict_process_cmd[key][1]))
	

def get_tty(pid,ppid,Pcmd,bash_vect,cmd):

	try:
		index = bash_vect.index(ppid)
	except:
		index = -1

	if Pcmd.find("bash")==0 and  index< 0:

		bash_vect.append(ppid)

		return "pts/" + str(bash_vect.index(ppid))

	elif Pcmd.find("bash")==0 and index >= 0:

		return "pts/" + str(bash_vect.index(ppid))


	elif cmd.find("bash")==0 :

		bash_vect.append(pid) 

		return "pts/"+str(bash_vect.index(pid))
		
	else:
		return '?'


def main():

	flag_all = sys.argv
	
	if len(flag_all) > 1:
	
		ps_print(ps(),flag_all[1])
	
	else:
 	
 		ps_print(ps(),"")


if __name__ == "__main__":
	
	main()